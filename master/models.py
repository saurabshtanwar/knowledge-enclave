from django.db import models
from packages.models import PackageDetails
from django.conf import settings
from django.utils import timezone
# Create your models here.


class State(models.Model):
    state_name = models.CharField(max_length=200)
    state_id = models.AutoField(primary_key=True)

    state_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class City(models.Model):
    city_Name = models.CharField(max_length=200)
    city_id = models.AutoField(primary_key=True)

    state_id = models.ForeignKey(State, on_delete=models.CASCADE, null=True)
    city_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class Tax(models.Model):
    tax_name = models.CharField(max_length=200)
    tax_id = models.AutoField(primary_key=True)

    symbol = models.CharField(max_length=200)
    tax_percentage = models.PositiveIntegerField()
    tax_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class Offer(models.Model):
    offer_name = models.CharField(max_length=20)
    offer_id = models.AutoField(primary_key=True)

    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)
    package_type = models.CharField(max_length=100, choices=(
        ('One Day Pakage', 'One Day Tour Pakage'), ('One Night Two Days', 'One Night Two Days'), ('Two Night Two Days', 'Two Night Two Days')), blank=True,)
    validity_from = models.DateField(null=True)
    validity_to = models.DateField(null=True)
    description = models.TextField(max_length=200, null=True)
    price = models.PositiveIntegerField(null=True)
    offer_price = models.PositiveIntegerField(null=True)
    # images
    offer_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class FAQ_category(models.Model):
    fcat_id = models.AutoField(primary_key=True)
    faq_category = models.CharField(max_length=20)
    fcat_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class FAQ(models.Model):
    faq_id = models.AutoField(primary_key=True)
    fcat = models.ForeignKey(FAQ_category, on_delete=models.CASCADE, null=True)
    faq_Question = models.TextField(max_length=2000, null=True)
    faq_answer = models.TextField(max_length=2000, null=True)
    faq_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class Feedback(models.Model):
    feedback_id = models.AutoField(primary_key=True)
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, null=True)
    user_image = models.ImageField(upload_to='images/', null=True)
    feedback = models.TextField(max_length=200)
    review_date = models.DateField(null=True)
    feedback_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)


class Payment_Method(models.Model):
    payM_id = models.AutoField(primary_key=True)
    payment_name = models.CharField(max_length=200)
    # image
    payM_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)
