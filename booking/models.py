from django.db import models
from django.conf import settings
from django.utils import timezone
from master.models import Payment_Method
from packages.models import *
from django.core.validators import RegexValidator
# Create your models here.


class Booking(models.Model):
    booking_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, null=True)
    #source = models.CharField(max_length=20)
    #destination = models.CharField(max_length=20)
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)
    arrival_date = models.DateField(null=True)
    departure_date = models.DateField(null=True)
    no_of_person = models.PositiveIntegerField(default=0)
    amount = models.PositiveIntegerField()
    no_of_rooms = models.PositiveIntegerField(default=0, null=True, blank=True)
    message = models.TextField(default="")
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(
        validators=[phone_regex], max_length=17, blank=True)  # Validators should be a list
    name = models.CharField(max_length=20, default='coustmer')
    paid = models.BooleanField(default=0)


class paidBooking(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()
    payment_method = models.ForeignKey(
        Payment_Method, on_delete=models.CASCADE)
    paid = models.PositiveIntegerField()
    remaining = models.PositiveIntegerField()
    fully_paid = models.BooleanField()
    txn_id = models.CharField(max_length=20, default='cash')
