# Generated by Django 4.0.4 on 2022-06-25 11:33

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0002_alter_booking_booking_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='message',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='booking',
            name='no_of_rooms',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='booking',
            name='phone_number',
            field=models.CharField(blank=True, max_length=17, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')]),
        ),
    ]
