# Generated by Django 4.0.4 on 2022-06-26 00:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0004_booking_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='no_of_rooms',
            field=models.PositiveIntegerField(blank=True, default=0, null=True),
        ),
    ]
