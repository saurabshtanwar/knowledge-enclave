from email.mime import image
from django.db import models
from django.conf import settings
from django.utils import timezone
from packages.models import *
from master.models import *
from blog.models import *
from home.models import *
# Create your models here.


class Header(models.Model):
    Header_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Header_to_Show(models.Model):
    toShow = models.ForeignKey(
        Header, on_delete=models.CASCADE, blank=True)


class Vision(models.Model):
    Team_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Name = models.CharField(max_length=1000, null=True)
    Facebook = models.CharField(
        max_length=200, null=True, default="https://www.facebook.com")
    Twitter = models.CharField(
        max_length=200, null=True, default="https://www.facebook.com")
    Linked_in = models.CharField(
        max_length=200, null=True, default="https://www.facebook.com")
    Instagram = models.CharField(
        max_length=200, null=True, default="https://www.facebook.com")


class Mission(models.Model):
    Mission_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Values(models.Model):
    Values_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Bottom(models.Model):
    Bottom_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)
