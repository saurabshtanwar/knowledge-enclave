# Generated by Django 4.0.4 on 2022-06-24 00:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0002_header_to_show'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vision',
            old_name='Vision_image',
            new_name='Team_image',
        ),
    ]
