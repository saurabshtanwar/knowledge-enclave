from modulefinder import packagePathMap
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm

from importlib.resources import Package
from zoneinfo import available_timezones
from django.shortcuts import render, redirect
from django.http import HttpResponse
from home.models import *
from about.models import *
from packages.models import *
from master.models import Feedback
from booking.models import *
from blog.models import Post
from django.contrib.auth import login
from django.contrib import messages
from home.form import SignUpForm, LoginForm, bookform


# Create your views here.

# Home
def index(request):
    Header_ = Header.objects.all()
    Package_ = PackageDetails.objects.filter(Package_status="Available")
    Package_One_Night_Two_Days = PackageDetails.objects.filter(
        ptype="One Night Two Days")
    Package_Two_Night_Two_Days = PackageDetails.objects.filter(
        ptype="Two Night Two Days")

    select_testimonial = Testimonial.objects.all()

    logo_ = logo.objects.all()
    Testimonials = Feedback.objects.all()
    post = Post.objects.all()

    return render(request, 'index-03.html', {
        "headers": Header_,
        "packages": Package_,
        "package_type_2": Package_One_Night_Two_Days,
        "package_type_3": Package_Two_Night_Two_Days,
        "Testimonials": select_testimonial,
        "post": post,
        "logo": logo_
    })

# About us


def about(request):
    Teams = Vision.objects.all()
    Values_ = Values.objects.all()
    Mission_ = Mission.objects.all()
    Header_ = Header.objects.all()
    Package_ = PackageDetails.objects.filter(Package_status="Available")
    Package_One_Night_Two_Days = PackageDetails.objects.filter(
        ptype="One Night Two Days")
    Package_Two_Night_Two_Days = PackageDetails.objects.filter(
        ptype="Two Night Two Days")

    select_testimonial = Testimonial.objects.all()

    logo_ = logo.objects.all()
    Testimonials = Feedback.objects.all()
    post = Post.objects.all()
    print(Values_)
    return render(request, 'about-us.html', {
        "headers": Header_,
        "packages": Package_,
        "package_type_2": Package_One_Night_Two_Days,
        "package_type_3": Package_Two_Night_Two_Days,
        "Testimonials": select_testimonial,
        "post": post,
        "logo": logo_,
        "mission": Mission_,
        "values": Values_,
        "teams": Teams
    })

# Register


def register_request(request):
    logo_ = logo.objects.all()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'sign-up.html', {'form': form, "logo": logo_})

# Login


def login_page(request):
    logo_ = logo.objects.all()
    form = LoginForm()
    message = ''
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password']
            )
            if user is not None:
                login(request, user)
                message = f'Hello {user.username}! You have been logged in'
                return redirect('index')
            else:
                message = 'Login failed!'
                print(message)
    return render(request, 'sign-in.html', context={'form': form, 'logo': logo_, 'message': message})

# Logout


def logout_user(request):
    logout(request)
    return redirect('login')

# Booknow


def Booknow(request):
    Package_ = PackageDetails.objects.filter(Package_status="Available")
    logo_ = logo.objects.all()
    if not request.user.is_authenticated:
        return redirect('login')
    current_user = request.user
    if request.method == 'POST':
        book = Booking()
        print('123')
        form = request.POST
        name = form['name']
        email = form['email']
        phone_number = form['phone_number']
        package = form['package']
        arrival_date = form['arrival_date']
        departure_date = form['departure_date']
        #no_of_rooms = form['no_of_rooms']
        no_of_person = form['no_of_person']
        mult = 1
        print(type(no_of_person))
        if int(no_of_person) % 2 == 0:
            mult = int(no_of_person)/2
        else:
            mult = (int(no_of_person)+1)/2
        message = form['message']
        book.name = name
        book.email = email
        book.phone_number = phone_number
        book.arrival_date = arrival_date
        book.departure_date = departure_date
        #book.no_of_rooms = no_of_rooms
        book.no_of_person = no_of_person
        book.message = message

        amount = 25000
        pak = PackageDetails.objects.filter(pname=package).first()
        if pak:
            book.package = pak
            amount = pak.offer_amount
        book.amount = amount * mult
        book.user = current_user
        book.save()
        bookin = Booking.objects.filter(user=current_user).last()
        return redirect('book2', bookin.booking_id)
    else:
        print("not working")
    return render(request, 'booking.html', context={'logo': logo_, 'packages': Package_})


def book2(request, booking_id):
    Package_ = PackageDetails.objects.filter(Package_status="Available")
    logo_ = logo.objects.all()
    payment_method = Payment_Method.objects.all()
    paidbooking = paidBooking()
    if request.method == 'POST':
        form = request.POST
        payment_m = form['payment_m']
        paidbooking.booking = Booking.objects.filter(
            booking_id=booking_id).last()
        paidbooking.amount = Booking.objects.filter(
            booking_id=booking_id).last().amount
        if payment_m == 'cash':
            paidbooking.paid = 0
            paidbooking.remaining = (Booking.objects.filter(
                booking_id=booking_id).last().amount)
            paidbooking.fully_paid = 0
            paidbooking.txn_id = "cash"
        else:
            paidbooking.paid = form['paid']
            paidbooking.remaining = int(Booking.objects.filter(
                booking_id=booking_id).last().amount) - int(form['paid'])
            paidbooking.fully_paid = 1
            paidbooking.txn_id = form['txn_id']
        paidbooking.payment_method = Payment_Method.objects.filter(
            payment_name=payment_m).last()
        paidbooking.save()
        book_info = Booking.objects.filter(
            booking_id=booking_id).last()
        book_info.paid = 1
        book_info.save()
        pb = paidBooking.objects.filter(booking=Booking.objects.filter(
            booking_id=booking_id).last()).last()
        return render(request, 'booking-03.html', context={'logo': logo_, 'packages': Package_, 'pb': pb, 'book': book_info})

    return render(request, 'booking-02.html', context={'logo': logo_, 'packages': Package_, 'pm': payment_method})
