from email.mime import image
from django.db import models
from django.conf import settings
from django.utils import timezone
from packages.models import *
from master.models import *
from blog.models import *
# Create your models here.


class Scroll_Image(models.Model):
    name = models.CharField(max_length=255, null=True)
    image = models.ImageField(upload_to='images/', null=True)
    default = models.BooleanField(default=False)


class Pakages_view_home(models.Model):
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)


class Tourist_attr_view_home(models.Model):
    attraction = models.ForeignKey(
        Tourist_Attractions, on_delete=models.CASCADE, null=True)


class Blog_view_home(models.Model):
    blog = models.ForeignKey(
        Post, on_delete=models.CASCADE, null=True)


class Testimonial(models.Model):
    testimonial = models.ForeignKey(
        Feedback, on_delete=models.CASCADE, null=True)


class Side_content(models.Model):
    Side_Image = models.ImageField(upload_to='images/')
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class logo(models.Model):
    Logo_Image = models.ImageField(upload_to='images/')
    Icon_Image = models.ImageField(upload_to='images/')
    Title = models.CharField(max_length=200, null=True)
    Tag_Line = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    facebook = models.CharField(max_length=200, null=True)
    twitter = models.CharField(max_length=200, null=True)
    linked_in = models.CharField(max_length=200, null=True)
    instagram = models.CharField(max_length=200, null=True)
