from django.contrib import admin
from django.urls import path, include
from home.views import book2, index
from home import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', index, name='index'),
    path('home', index, name='index'),
    path('home/', index, name='index'),
    path("register", views.register_request, name="register"),
    path("register/", views.register_request, name="register"),
    path('login', views.login_page, name='login'),
    path('login/', views.login_page, name='login'),
    path('logout', views.logout_user, name='logout'),
    path('logout/', views.logout_user, name='logout'),
    path('about', views.about, name="about"),
    path('about/', views.about, name="about"),
    path('booking', views.Booknow, name="booking"),
    path('booking/', views.Booknow, name="booking"),
    path('booking-02/<int:booking_id>', views.book2, name="book2"),
]
