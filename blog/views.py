from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm

from importlib.resources import Package
from zoneinfo import available_timezones
from django.shortcuts import render, redirect
from django.http import HttpResponse
from home.models import *
from about.models import *
from packages.models import *
from master.models import Feedback
from blog.models import Post, BlogPost
from django.contrib.auth import login
from django.contrib import messages
from home.form import SignUpForm, LoginForm

# Create your views here.
from blog.models import *


def Single_blog(request):
    logo_ = logo.objects.all()
    Testimonials = Feedback.objects.all()
    post = BlogPost.objects.all()
    return render(request, 'blog-standard.html', {
        "post": post,
        "logo": logo_
    })


def blog(request, id):
    logo_ = logo.objects.all()
    Testimonials = Feedback.objects.all()
    posts = BlogPost.objects.all()
    single_post = BlogPost.objects.filter(id=id)
    return render(request, 'blog-single.html', {
        "para": id,
        "post": single_post,
        "posts": posts,
        "logo": logo_
    })
