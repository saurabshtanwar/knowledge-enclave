from django.contrib import admin
from django.urls import path, include
from blog import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.Single_blog, name='blog'),
    path('viewblog/<int:id>', views.blog, name="single_blog"),
    path('viewblog/<int:id>/', views.blog, name="single_blog")
]
