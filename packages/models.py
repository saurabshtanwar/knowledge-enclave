from pyexpat import model
from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.


class PackageDetails(models.Model):
    pname = models.CharField(max_length=20, primary_key=True)
    ptype = models.CharField(max_length=100, choices=(
        ('One Day Pakage', 'One Day Tour Pakage'), ('One Night Two Days', 'One Night Two Days'), ('Two Night Two Days', 'Two Night Two Days')), blank=True,)
    pdetail = models.TextField(max_length=200, null=True)
    food_type = models.CharField(max_length=100, choices=(
        ('Veg', 'Veg'), ('Non-veg', 'Non-veg')), blank=True,)
    pdestination = models.CharField(max_length=200, null=True)
    amount = models.PositiveIntegerField(null=True)
    offer_amount = models.PositiveIntegerField(null=True)
    main_image = models.ImageField(upload_to='images/')
    Package_status = models.CharField(max_length=200, choices=(
        ('Available', 'Available'), ('Unavailable', 'Unavailable'), ('Closed', 'Closed')), blank=True)

    def __name__(self):
        return(self.pname)


class Package_Image(models.Model):
    name = models.CharField(max_length=255)
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)
    image = models.ImageField(upload_to='images/')
    default = models.BooleanField(default=False)


class Tourist_Attractions(models.Model):
    name = models.CharField(max_length=255, null=True)
    id = models.AutoField(primary_key=True)
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)
    details = models.TextField(max_length=500, null=True)


class Tourist_Attractions_Image(models.Model):
    name = models.CharField(max_length=255)
    package = models.ForeignKey(
        Tourist_Attractions, on_delete=models.CASCADE, null=True)
    image = models.ImageField(upload_to='images/')
    default = models.BooleanField(default=False)
